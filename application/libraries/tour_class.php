<?php

class tour_class {

    public $output = '';

    public function __construct() {
        $out = '';
        $tmp = '        
        <div class="col-sm-4">
            <a href="#url_big#" target="_blank" class="thumbnail">
                <img style="height:280px; width:100%;" src="#url#">
                <p class="gh-text-shadow">#title#</p>
            </a>
        </div>  
';
        $my = new mysql_class;
        $my->ex_sql("select * from tour order by `order`", $q);
        foreach($q as $r)
        {
            $t = str_replace("#url#", $r['url'], $tmp);
            $t = str_replace("#url_big#", $r['url_big'], $t);
            $t = str_replace("#title#", $r['title'], $t);
            $t = str_replace("#des#", $r['des'], $t);
            $out .= $t;
        }
        $this->output = $out;
        
    }

    public function search($title,$to_city,$hotel_name)
    {
        $wer = '';
        $title = trim($title);
        $to_city = trim($to_city);
        $hotel_name = trim($hotel_name); 
        if($title!='')
        {
            $wer = " where  `title` like '%$title%' ";
        }
        if($to_city!='')
        {
            $wer .= (($wer != '')?' and ':' where ') . " `to_city` = '$to_city' ";
        }
        if($hotel_name!='')
        {
            $wer .= (($wer != '')?' and ':' where ') . " `hotel_name` like '%$hotel_name%' ";
        }
        $my = new mysql_class;
        $my->ex_sql("select * from tour $wer order by `order`", $q);
//        echo "select * from tour $wer order by `order`";
        return($q);
    }

}
