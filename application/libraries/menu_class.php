<?php

class menu_class {

    public $output = '';

    public function __construct($is_admin = FALSE) {
        if ($is_admin) {
            $this->output = $this->getAdminMenu();
        } else {
            $this->output = $this->getSubMenu();
        }
    }

    public function getSubMenu($parent_id = 0) {
        $out = '';
        $my = new mysql_class;
        $my->ex_sql("select id,name,is_page from menu where parent_id = $parent_id order by order_number", $q);
        foreach ($q as $r) {
            $url = '#';
            if ((int) $r['is_page'] != 0) {
                $url = site_url() . 'page_loader?id=' . $r['id'] . '';
            }
            $out .= '<li><a href="' . $url . '">' . (($r['id'] == 0) ? '<img style="width: 15px; height: 15px; margin-left: 3px;" src="' . asset_url() . 'images/img/home.png">' : '') . $r['name'];
            $sub = $this->getSubMenu($r['id']);
            if ($sub != '') {
                $out .= (($parent_id == 0) ? '<span class="caret"></span>' : '<span style="float: left;"><img src="' . asset_url() . 'images/img/left.png"></span>') . '</a><ul>' . $sub . '</ul>';
            } else {
                $out .='</a>';
            }
            $out .= '</li>';
        }
        return($out);
    }

    public function getAdminMenu($parent_id = 0) {
        $out = '';
        $my = new mysql_class;
        $my->ex_sql("select * from menu where parent_id = $parent_id and (parent_id<>0 || name<>'صفحه اصلی') order by order_number", $q);
        foreach ($q as $r) {
            $url = '#';
            if ((int) $r['is_page'] != 0 || (int) $r['parent_id'] == 0) {
                $url = site_url() . 'admin?page_id=' . $r['id'] . '';
            }
            $out .= '<li><a href="' . $url . '">' . $r['name'];
            $sub = $this->getAdminMenu($r['id']);
            if ($sub != '') {
                $out .= '</a><ul>' . $sub . '</ul>';
            } else {
                $out .='</a>';
            }
            $out .= '</li>';
        }
        return($out);
    }

}
