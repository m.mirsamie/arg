<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
$out = '-1';
$table = isset($_REQUEST['table']) ? trim($_REQUEST['table']) : '';
if ($table != '') {
    $out = '';
    $tours = json_decode($table);
    $my = new mysql_class;
    $my->ex_sqlx("truncate table tour");
    foreach ($tours as $tour) {
        $query = "insert into tour (`title`, `url`, `des`, `from_city`, `to_city`, `price`, `nights`, `hotel_name`, `stars`, `services`, `airline`,`res_link`,`url_big`) values";
        $query .= "('" . $tour->name . "','" . $tour->photo . "','" . $tour->description . "','" . $tour->source . "','" . $tour->destination . "','" . $tour->price . "','" . $tour->nights . "','" . $tour->hotel_name . "','" . $tour->stars . "','" . $tour->services . "','" . $tour->airline . "','" . $tour->link . "','" . $tour->photo2 . "')";
        $ln = $my->ex_sqlx($query,FALSE);
        $out .= (($out!='')?',':'').$my->insert_id($ln);
        $my->close($ln);
//        $out = $query;
    }
}
die($out);