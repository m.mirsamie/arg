<?php
//$dat = 0;
//if (isset($_REQUEST['dat'])) {
//    $dat = (int) $_REQUEST['dat'];
//}
//$result_fare = search_class::loadLowFare($dat);
//$results = $result_fare["data"];
//foreach ($results as $i => $res) {
//    $results[$i]['from_city_small'] = $this->inc_model->substrH(city_class::loadByIata($res['from_city']), 5);
//    $results[$i]['to_city_small'] = $this->inc_model->substrH(city_class::loadByIata($res['to_city']), 5);
//    $results[$i]['from_city_name'] = city_class::loadByIata($res['from_city']);
//    $results[$i]['to_city_name'] = city_class::loadByIata($res['to_city']);
//    $results[$i]['price_monize'] = $this->inc_model->monize($res['price']);
//}
//$sign = ($dat < 0) ? ' - ' : ' + ';
//$dat = abs($dat);
//$result_fare['tarikh'] = $this->inc_model->perToEnNums(jdate("d-m-Y", strtotime(date("Y-m-d") . $sign . $dat . ' day')));
//$result_fare["data"] = $results;
//if (isset($_REQUEST['dat'])) {
//    die(json_encode($result_fare));
//}
$m = new menu_class();
//var_dump($m);
$tour_obj = new tour_class();
$tours = $tour_obj->output;

function get_web_page($url) {
    $options = array(
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => false, // don't return headers
        CURLOPT_FOLLOWLOCATION => true, // follow redirects
        CURLOPT_ENCODING => "", // handle all encodings
        CURLOPT_USERAGENT => "spider", // who am i
        CURLOPT_AUTOREFERER => true, // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
        CURLOPT_TIMEOUT => 120, // timeout on response
        CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
        CURLOPT_SSL_VERIFYPEER => false     // Disabled SSL Cert checks
    );

    $ch = curl_init($url);
    curl_setopt_array($ch, $options);
    $content = curl_exec($ch);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);
    curl_close($ch);

    $header['errno'] = $err;
    $header['errmsg'] = $errmsg;
    $header['content'] = $content;
    return $header;
}

/*
  $tour_tmp = <<<TMP
  <div class="col-sm-4">
  <a href="#res_link#" class="thumbnail">
  <img src="#url#">
  <p class="gh-text-shadow">#title#</p>
  </a>
  </div>
  TMP;
  $my = new mysql_class;
  $my->ex_sql("select * from tour order by price limit 6", $q);
  foreach ($q as $r) {
  $tour = str_replace("#res_link#", $r['res_link'], $tour_tmp);
  $tour = str_replace("#url#", $r['url'], $tour);
  $tour = str_replace("#title#", $r['title'], $tour);
  $tours .= $tour;
  }
 * 
 */
$cont = get_web_page("http://gohar.arg90.ir/gohar/city.php");
$cities = json_decode($cont['content']);
$my = new mysql_class;
$my->ex_sqlx("truncate table city");
foreach ($cities as $city) {
    $my->ex_sqlx("insert into city (iata,name,en_name) values ('" . $city->iata . "','" . $city->fa_name . "','" . $city->en_name . "')");
}
?>
 <div class="container-fluid">
<!--slide bar-->
<div class="row" style="background-color: #fff;">
    <div class="col-sm-1"></div>
    <div class="col-sm-10 gh-slider hidden-sm hidden-xs">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators hidden-xs hidden-sm hidden-md">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>   
            <div class="carousel-inner">
                <div class="item active">
                    <img src="<?php echo asset_url(); ?>images/img/slider1.png">
                </div>
                <div class="item">
                    <img src="<?php echo asset_url(); ?>images/img/slider2.png">
                    <div class="carousel-caption"></div>
                </div>
                <div class="item">
                    <img src="<?php echo asset_url(); ?>images/img/slider4.png">
                    <div class="carousel-caption"></div>
                </div>
            </div>
        </div>
    </div>


    <!--search-box-->
    <div class="col-lg-4 col-md-5 gh-search-box hidden-sm hidden-xs">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href=".flight"><img title="پرواز" src="<?php echo asset_url(); ?>images/img/flight.png"></a></li>
            <li><a data-toggle="tab" href=".tour"><img title="تور" src="<?php echo asset_url(); ?>images/img/tour.png"></a></li>
            <li><a data-toggle="tab" href=".hotel"><img title="هتل" src="<?php echo asset_url(); ?>images/img/hotel.png"></a></li>
        </ul>
        <div class="tab-content gh-search-box-body">
            <div class="tab-pane fade in active flight">
                <form style="padding-bottom: 10px;" method="post" action="<?php echo site_url(); ?>search">
                    <table>
                        <span style="text-align: center; width: 100%; display: block;"><جستجوی بلیط></span>
                        <tr>
                            <td colspan="2">
                                <input type="radio" name="way" value="one" checked>یک طرفه
                                <input type="radio" name="way" value="two">رفت و برگشت
                            </td>
                        </tr>
                        <tr>
                            <td>
                                مبداء
                                <select style="width: 100%; margin-bottom: 10px;" class="gh-city sel1" name="from_city" onchange="fn(this);">
                                    <option value="">
                                        <?php echo city_class::loadAll(); ?>
                                    </option>
                                </select>
                            </td>
                            <td>
                                مقصد
                                <select style="width: 100%; margin-bottom: 10px;" class="gh-city sel2" name="to_city">
                                    <option value=""><?php echo city_class::loadAll(); ?></option>
                                </select>
                            </td>
                        </tr>
<!--                        <tr>
                            <td>
                                ایرلاین
                                <select class="form-control" name="airline" disabled>
                                    <option value=""></option>
                                </select>
                            </td>
                            <td>
                                کلاس
                                <select class="form-control" name="fclass" disabled>
                                    <option  style="width: 40%" value=""></option>
                                </select>
                            </td>
                        </tr>-->
                        <tr>
                            <td>
                                از تاریخ
                                <input type="text" name="aztarikh" class="form-control dateValue2" id="aztarikh" placeholder="از تاریخ">
                            </td>
                            <td id="tatarikh-td">
                                تا تاریخ
                                <input type="text" name="tatarikh" class="form-control dateValue2" id="tatarikh" placeholder="تا تاریخ">
                            </td>
                        </tr>
<!--                        <tr>
                            <td colspan="2" class="gh-search-wtf">
                                <div style="width: 33%;">
                                    بزرگسال > 12 سال
                                    <select class="form-control" disabled>
                                        <option value="">1</option>
                                        <option value="">2</option>
                                        <option value="">3</option>
                                        <option value="">4</option>
                                        <option value="">5</option>
                                        <option value="">6</option>
                                        <option value="">7</option>
                                        <option value="">8</option>
                                        <option value="">9</option>
                                    </select>
                                </div>
                                <div style="width: 34%;">
                                    کودک 2 تا 12 سال
                                    <select class="form-control" disabled>
                                        <option value="">0</option>
                                        <option value="">1</option>
                                        <option value="">2</option>
                                        <option value="">3</option>
                                        <option value="">4</option>
                                        <option value="">5</option>
                                        <option value="">6</option>
                                        <option value="">7</option>
                                        <option value="">8</option>
                                    </select></div>
                                <div style="width: 33%;">
                                    نوزاد < 2
                                    <select class="form-control" disabled>
                                        <option value="">0</option>
                                        <option value="">1</option>
                                        <option value="">2</option>
                                        <option value="">3</option>
                                        <option value="">4</option>
                                        <option value="">5</option>
                                        <option value="">6</option>
                                        <option value="">7</option>
                                        <option value="">8</option>
                                    </select></div>
                            </td>
                        </tr>-->
                        <tr>
                            <td colspan="2">
                                <button style="margin-top: 15px;" type="submit" class="btn btn-block">جستجو</button>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="tab-pane fade tour">
                <form method="post" action="<?php echo site_url(); ?>tour">
                    <table>
                        <span style="text-align: center; width: 100%; display: block;"><جستجوی تور></span>
                        <tr>
                            <td style="width: 100%;">
                                عنوان
                                <input class="form-control" name="tour-title" type="text" placeholder="عنوان تور">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                مقصد
                                <select class="form-control" style="width: 100%;" name="tour-maghsad">
                                    <option value="">
                                        <?php echo city_class::loadAll(); ?>
                                    </option>
                                </select>
                            </td>
                        <tr>
                            <td style="width: 100%;">
                                هتل
                                <input class="form-control" name="tour-hotel" type="text" placeholder="انتخاب هتل">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button style="margin-top: 10px;" type="submit" class="btn btn-block">جستجو</button>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="tab-pane fade hotel" >
                <?php
                //require('tdm_hotel.php');
				?>
            </div>
        </div>
    </div>
    <!--search-box-large-->
    <!--search-box-small-->
    <div class="col-sm-10 col-xs-12 gh-search-box visible-sm visible-xs" style="position: static; margin: 10px 0;">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href=".flight"><img title="پرواز" src="<?php echo asset_url(); ?>images/img/flight.png"></a></li>
            <li><a data-toggle="tab" href=".tour"><img title="تور" src="<?php echo asset_url(); ?>images/img/tour.png"></a></li>
            <li><a data-toggle="tab" href=".hotel"><img title="هتل" src="<?php echo asset_url(); ?>images/img/hotel.png"></a></li>
        </ul>
        <div class="tab-content gh-search-box-body">
            <div class="tab-pane fade in active flight">
                <form method="post" action="search.php">
                    <table>
                        <span style="text-align: center; width: 100%; display: block;"><جستجوی بلیط></span>
                        <tr>
                            <td colspan="2">
                                <input type="radio" name="way" value="one" checked>یک طرفه
                                <input type="radio" name="way" value="two">رفت و برگشت
                            </td>
                        </tr>
                        <tr>
                            <td>
                                مبداء
                                <select style="width: 100%;" class="gh-city sel1" name="from_city" onchange="fn(this);">
                                    <option value="">
                                        <?php echo city_class::loadAll(); ?>
                                    </option>
                                </select>
                            </td>
                            <td>
                                مقصد
                                <select style="width: 100%;" class="gh-city sel2" name="to_city">
                                    <option value=""><?php echo city_class::loadAll(); ?></option>
                                </select>
                            </td>
                        </tr>
<!--                        <tr>
                            <td>
                                ایرلاین
                                <select class="form-control" name="airline" disabled>
                                    <option value=""></option>
                                </select>
                            </td>
                            <td>
                                کلاس
                                <select class="form-control" name="fclass" disabled>
                                    <option  style="width: 40%" value=""></option>
                                </select>
                            </td>
                        </tr>-->
                        <tr>
                            <td>
                                از تاریخ
                                <input type="text" name="aztarikh" class="form-control dateValue2" id="aztarikh" placeholder="از تاریخ">
                            </td>
                            <td id="tatarikh-td">
                                تا تاریخ
                                <input type="text" name="tatarikh" class="form-control dateValue2" id="tatarikh" placeholder="تا تاریخ">
                            </td>
                        </tr>
<!--                        <tr>
                            <td colspan="2" class="gh-search-wtf">
                                <div style="width: 33%;">
                                    بزرگسال > 12 سال
                                    <select class="form-control" disabled>
                                        <option value="">1</option>
                                        <option value="">2</option>
                                        <option value="">3</option>
                                        <option value="">4</option>
                                        <option value="">5</option>
                                        <option value="">6</option>
                                        <option value="">7</option>
                                        <option value="">8</option>
                                        <option value="">9</option>
                                    </select>
                                </div>
                                <div style="width: 34%;">
                                    کودک 2 تا 12 سال
                                    <select class="form-control" disabled>
                                        <option value="">0</option>
                                        <option value="">1</option>
                                        <option value="">2</option>
                                        <option value="">3</option>
                                        <option value="">4</option>
                                        <option value="">5</option>
                                        <option value="">6</option>
                                        <option value="">7</option>
                                        <option value="">8</option>
                                    </select></div>
                                <div style="width: 33%;">
                                    نوزاد < 2
                                    <select class="form-control" disabled>
                                        <option value="">0</option>
                                        <option value="">1</option>
                                        <option value="">2</option>
                                        <option value="">3</option>
                                        <option value="">4</option>
                                        <option value="">5</option>
                                        <option value="">6</option>
                                        <option value="">7</option>
                                        <option value="">8</option>
                                    </select></div>
                            </td>
                        </tr>-->
                        <tr>
                            <td colspan="2">
                                <button type="submit" class="btn btn-block">جستجو</button>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="tab-pane fade tour">
                <div class="tab-pane fade tour">
                    <form method="post" action="<?php echo site_url(); ?>tour">
                        <table>
                            <span style="text-align: center; width: 100%; display: block;"><جستجوی تور></span>
                            <tr>
                                <td style="width: 100%;">
                                    عنوان
                                    <input class="form-control" name="tour-title" type="text" placeholder="عنوان تور">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    مقصد
                                    <select class="form-control" style="width: 100%;" name="tour-maghsad">
                                        <option value="">
                                            <?php echo city_class::loadAll(); ?>
                                        </option>
                                    </select>
                                </td>
                            <tr>
                                <td style="width: 100%;">
                                    هتل
                                    <input class="form-control" name="tour-hotel" type="text" placeholder="انتخاب هتل">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <button style="margin-top: 10px;" type="submit" class="btn btn-block">جستجو</button>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
            <div class="tab-pane fade hotel">
                در حال آماده سازی ...
            </div>
        </div>
    </div>
    <!--search-box-small-->
    <div class="col-sm-1"></div>
</div>
<!--slide bar-->

<!--middle-->
<div class="row" style="background-color: #f3f3f3;">
    <div class="col-sm-1"></div>
    <div class="col-sm-10 gh-middle">
        <?php echo $tours; ?>
    </div>
    <div class="col-sm-1"></div>
</div>
<!--middle-->


<!--contact-us-->
<div class="row" style="background-color: #c3d2d9;">
    <div class="col-sm-1"></div>
    <div class="col-sm-10 gh-contact-us">
        <div class="col-sm-6 gh-contact-us-body">
            <form method="post">
                <table>
                    <tr>
                        <td>
                            <header><img class="gh-contact-icon" src="<?php echo asset_url(); ?>images/img/contact.png">ارسال نظرات</header>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span style="margin-right: 10px;">موضوع :</span>
                            <input type="radio" name="subject-title" value="flight" checked>پرواز
                            <input type="radio" name="subject-title" value="tour">تور
                            <input type="radio" name="subject-title" value="hotel">هتل
                            <input type="radio" name="subject-title" value="other">متفرقه
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" class="form-control" placeholder="نام *">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="email" class="form-control" placeholder="پست الکترونیک *">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" class="form-control" placeholder="شماره تماس">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <textarea style="resize: none; height: 60px;" class="form-control" placeholder="دیدگاه *"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <button type="submit" class="btn btn-success">ثبت</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>

        <div class="col-sm-6 gh-contact-us-body">
            <form method="post">
                <table>
                    <tr>
                        <td>
                            <header>باشگاه مشتریان <span>(ثبت تورهای لحظه آخری)</span></header>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 3px 0;">
                            <input type="text" class="form-control" placeholder="نام*">
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 3px 0;">
                            <input type="text" class="form-control" placeholder="نام خانوادگی*">
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 3px 0;">
                            <input type="email" class="form-control" placeholder="پست الکترونیک *">
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 3px 0;">
                            <input type="text" class="form-control" placeholder="شماره موبایل *">
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 8px 0;">
                            <span style="float: right; text-align: center;">نوع تور : 
                                <input type="radio" name="tour" value="in" checked>داخلی</option>
                                <input type="radio" name="tour" value="out">خارجی</option>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <button type="submit" class="btn btn-success">ثبت</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <div class="col-sm-1"></div>
    </div>
</div>
</div>
<!--contact-us-->
<script>
    $(window).scroll(function () {
        if ($(this).scrollTop()) {
            $('#toTop').fadeIn();
        } else {
            $('#toTop').fadeOut();
        }
    });

    $("#toTop").click(function () {
        //1 second of animation time
        //html works for FFX but not Chrome
        //body works for Chrome but not FFX
        //This strange selector seems to work universally
        $("html, body").animate({scrollTop: 0}, 1000);
    });
    //-------Menu animation------------
    $("div.gh-menu-large li").hover(function () {
        $(this).find('> ul').stop().show(200);
    });
    $("div.gh-menu-large li").mouseleave(function () {
        var obj = $(this);
        if (!obj.parent().parent().hasClass('gh-menu-large'))
        {
            obj.parent().find('ul').hide();
        }
        else
        {
            $("div.gh-menu-large > ul > li > ul").hide();
        }
    });
    //-------Menu animation------------
</script>