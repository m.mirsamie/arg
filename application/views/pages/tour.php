<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
if (!isset($_REQUEST['tour-title']) || !isset($_REQUEST['tour-maghsad']) || !isset($_REQUEST['tour-hotel'])) {
    redirect('home');
}
$tourTitle = $_REQUEST['tour-title'];
$tourMaghsad = $_REQUEST['tour-maghsad'];
$tourHotel = $_REQUEST['tour-hotel'];
$resultTour = tour_class::search($tourTitle, $tourMaghsad, $tourHotel);

$tmp = <<<mmcomp
<div class="gh-sr-result col-sm-12" style="padding: 0; padding-bottom:10px;">
        <table>
            <tr>
                <td style="font-size:20px;">
                    #tour-title#
                </td>
                <td style="padding: 0;">
                            <td>
                                مقصد : #tour-maghsad#
                            </td>
                            <td>
                               زمان : #tour-date# شب
                            </td>
                </td>
                <td><img style="width:100px; height: 100px; padding: 5px 0;" src="#tour-url#"></td>
                <td style="background-color: #f0f0f0; width: 250px;">
                    <p style="font-size: 18px; padding-right: 15px; padding-top: 5px;">#price# تومان</p>
                    <p><a style="margin-top: 6px;" class="yourButton" target='_blank' href="#site#">انتخاب</a><br></p>
                </td>
            </tr>
        </table>
</div>
mmcomp;

$tourRes = '';

foreach ($resultTour as $tour) {
    $res = str_replace("#tour-title#", $tour['title'], $tmp);
    $res = str_replace("#tour-maghsad#", city_class::loadByIata($tour['to_city']), $res);
    $res = str_replace("#tour-date#", $tour['nights'], $res);
    $res = str_replace("#tour-url#", $tour['url'], $res);
    $res = str_replace("#price#", $tour['price'], $res);
    $res = str_replace("#site#", $tour['res_link'], $res);
    $tourRes .= $res;
}
?>

<div class="hidden-xs" id='toTop'><span class="glyphicon glyphicon-circle-arrow-up"></span></div>
<div class="row" style="margin-top: 10px;">
    <div class="col-sm-2"></div>
    <div class="col-sm-1"></div>
</div>

<div class="row" style="margin-top: 10px;">
    <div class="col-sm-2"></div>
    <div class="gh-sp-body col-sm-8 gh-border-radius" style="margin-bottom: 10px;">
        <div class="row" style="padding: 5px;">
            <!--search result-->
            <div class="col-sm-12">
                <div class="mm-res-ha">
                </div>
            </div>
            <!--search result-->
        </div>
        <div class="row">
            <div class="col-sm-12" style="padding: 5px;">
                <?php echo $tourRes ?>
            </div>
        </div>
    </div>
    <div class="col-sm-2"></div>
</div>


<div class="gh-sabr" style="display:none;background-image: url('<?php echo asset_url(); ?>images/img/search-box-header.png');width:100%;height: 100%;position: fixed;top:0px;left:0px;text-align: center;">
    <img style="width: 300px;margin-top: 200px;" src="<?php echo asset_url(); ?>images/img/loading_big.gif">
</div>
<script>
    $(window).scroll(function () {
        if ($(this).scrollTop()) {
            $('#toTop').fadeIn();
        } else {
            $('#toTop').fadeOut();
        }
    });

    $("#toTop").click(function () {
        //1 second of animation time
        //html works for FFX but not Chrome
        //body works for Chrome but not FFX
        //This strange selector seems to work universally
        $("html, body").animate({scrollTop: 0}, 1000);
    });
</script>