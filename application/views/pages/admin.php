<?php
if (!isset($_SESSION['user_id'])) {
    redirect("login");
    //echo "No session";
}

function loadImage($url) {
    $url = trim($url);
    $out = '---';
    if ($url != '') {
        $out = "<a href='$url' target='_blank'><img height='20px' src='$url' ></a>";
    }
    return($out);
}

function loadMenus() {
    $out = array(0 => '----');
    $my = new mysql_class;
    $my->ex_sql("select id,name from menu order by parent_id,name", $q);
    foreach ($q as $r) {
        $out[(int) $r['id']] = $r['name'];
    }
    return($out);
}

function addFn($gname, $table, $fields, $column) {
    $out = FALSE;
    if ((int) $fields['parent_id'] > 0) {
        $my = new mysql_class;
        $my->ex_sqlx("insert into $table (name,parent_id,is_page) values ('" . $fields['name'] . "'," . $fields['parent_id'] . "," . $fields['is_page'] . ")");
        $out = TRUE;
    }
    return($out);
}

function editFn($table, $id, $field, $val, $fn, $gname) {
    $out = FALSE;
    if ($field != 'parent_id' || ($field == 'parent_id' && $val > 0)) {
        $my = new mysql_class;
        $my->ex_sqlx("update $table set `$field` = '$val' where id = $id");
        $out = TRUE;
    }
    return($out);
}

function deleteFn($table, $id, $gname) {
    $my = new mysql_class;
    $my->ex_sql("select id,parent_id from menu where id in ($id)", $q);
    $oks = 0;
    foreach ($q as $r) {
        if ((int) $r['parent_id'] > 0) {
            $my->ex_sqlx("delete from menu where id = " . $r['id']);
            $oks++;
        }
    }
    $out = ($oks == count($q) && $q > 0);
    return($out);
}

$content = '';
$js = '';
$img_state = '';
$my = new mysql_class;
if (isset($_FILES['url'])) {
    $tmp_target_path = "assets/images/ck_img";
    $ext = explode('.', basename($_FILES['url']['name']));
    $ext = $ext[count($ext) - 1];
    if (strtolower($ext) == 'jpg' || strtolower($ext) == 'png' || strtolower($ext) == 'gif') {
        $target_path = $tmp_target_path . "/" . basename($_FILES['url']['name']);
        if (move_uploaded_file($_FILES['url']['tmp_name'], $target_path)) {
            $img_state = asset_url() . "images/ck_img/" . basename($_FILES['url']['name']);
            if (isset($_REQUEST['title'])) {
                $my = new mysql_class;
                $query = "insert into tour (url,title,des) values ('" . $img_state . "','" . $_REQUEST['title'] . "','" . $_REQUEST['des'] . "')";
                $my->ex_sqlx($query);
            }
        }
    }
}

if (isset($_REQUEST['page_id'])) {
    $page_id = (int) $_REQUEST['page_id'];
    $page_content = '';
    $page = '----';
    $my->ex_sql("select name from menu where id = $page_id", $p);
    if (isset($p[0])) {
        $page = $p[0]['name'];
    }
    if (isset($_REQUEST['page_content'])) {
        $page_content = $_REQUEST['page_content'];
        $my->ex_sqlx("update content set content = '$page_content' where menu_id = $page_id");
        $my->ex_sqlx("update menu set is_page = 1 where id = $page_id");
    }
    $my->ex_sql("select content from content where menu_id = $page_id", $q);
    if (isset($q[0])) {
        $pcontent = $q[0]['content'];
    } else {
        $my->ex_sqlx("insert into content (menu_id,content) values ($page_id,'$page_content')");
        $pcontent = '';
    }
    $content = '<form method="post" enctype="multipart/form-data">' . "\n";
    $content .= '<h3>' . $page . '</h3>';
    $content .= '<input id="input-1" type="file" class="file" name="url">' . "\n";
    $content .= '<div>' . $img_state . '</div>';
    $content .= '<textarea name="page_content">' . $pcontent . '</textarea>' . "\n";
    $content .= '<input type="hidden" name="page_id" value="' . $page_id . '" >' . "\n";
    $content .= '<button>ذخیره</button>' . "\n";
    $content .= '</form>';
    $js = "CKEDITOR.replace('page_content');";
} /* else if (isset($_REQUEST['tour'])) {
  $content = '<form method="post" enctype="multipart/form-data"><input name="title" placeholder="موضوع" class="form-control" ><textarea name="des" placeholder="توضیحات" class="form-control" ></textarea><input id="input-1" type="file" class="file" name="url"><button class="btn btn-danger pull-left">ثبت</button></form>';
  $gname1 = 'gname_ad';
  $input = array($gname1 => array('table' => 'tour', 'div' => 'grid_tour'));
  $xgrid1 = new xgrid($input, site_url() . 'admin?');
  $xgrid1->column[$gname1][0]['name'] = '';
  $xgrid1->column[$gname1][1]['name'] = 'موضوع';
  $xgrid1->column[$gname1][2]['name'] = 'تصویر';
  $xgrid1->column[$gname1][2]['access'] = 1;
  $xgrid1->column[$gname1][2]['cfunction'] = array('loadImage');
  $xgrid1->column[$gname1][3]['name'] = 'توضیحات';
  $xgrid1->eRequest[$gname1] = array("tour" => 1);
  $xgrid1->pageRows[$gname1] = 10;
  $xgrid1->canAdd[$gname1] = FALSE;
  $xgrid1->canEdit[$gname1] = TRUE;
  $xgrid1->canDelete[$gname1] = TRUE;
  $out = $xgrid1->getOut($_REQUEST);
  if ($xgrid1->done)
  die($out);
  $js = <<<JSS
  var ggname_ad = '$gname1';
  var args = $xgrid1->arg;
  intialGrid(args);
  JSS;
  } */ else if (isset($_REQUEST['menu'])) {
    $content = '';
    $gname1 = 'gname_ad';
    $input = array($gname1 => array('table' => 'menu', 'div' => 'grid_tour'));
    $xgrid1 = new xgrid($input, site_url() . 'admin?');
    $xgrid1->whereClause[$gname1] = " parent_id > 0";
    $xgrid1->column[$gname1][0]['name'] = '';
    $xgrid1->eRequest[$gname1] = array("menu" => 1);
    $xgrid1->column[$gname1][1]['name'] = 'موضوع';
    $xgrid1->column[$gname1][2]['name'] = 'والد';
    $xgrid1->column[$gname1][2]['clist'] = loadMenus();
    $xgrid1->column[$gname1][3]['name'] = 'متن دارد';
    $xgrid1->column[$gname1][3]['clist'] = array('ندارد', 'دارد');
    $xgrid1->pageRows[$gname1] = 10;
    $xgrid1->canAdd[$gname1] = TRUE;
    $xgrid1->canEdit[$gname1] = TRUE;
    $xgrid1->canDelete[$gname1] = TRUE;
    $xgrid1->addFunction[$gname1] = 'addFn';
    $xgrid1->editFunction[$gname1] = 'editFn';
    $xgrid1->deleteFunction[$gname1] = 'deleteFn';
    $out = $xgrid1->getOut($_REQUEST);
    if ($xgrid1->done)
        die($out);
    $js = <<<JSS
    var ggname_ad = '$gname1';
    var args = $xgrid1->arg;
    intialGrid(args);
JSS;
}
$m = new menu_class(TRUE);
$upmenu = $m->output;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-3 gh-panel">
            <ul>
                <li style="text-align: center; border-bottom: 2px solid #cbcbcb; line-height: 40px; color: #fff;">آپشن های مدیریت</li>
                <li><a href="#">منوی بالا</a>
                    <ul>
                        <?php echo $upmenu; ?>
                    </ul>
                </li>
                <!--<li><a href="<?php echo site_url(); ?>admin?tour=1">تورها (صفحه اصلی)</a></li>-->
                <li><a href="<?php echo site_url(); ?>admin?menu=1">مدیریت منو</a></li>
                <li><a href="<?php echo site_url(); ?>login">خروج</a></li>
            </ul>
        </div>
        <div class="col-sm-7" id="base" style="margin-top: 20px;">
            <?php echo $content; ?>
            <div id="grid_tour"></div>
        </div>
        <div class="col-sm-1"></div>
    </div>
</div>
<script>
<?php echo $js; ?>
</script>