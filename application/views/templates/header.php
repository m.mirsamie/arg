<!DOCTYPE html>
<html>
    <head>
        <?php
        $this->load->helper('html');
        $this->load->helper('url');
        echo meta($meta);
        ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            <?php echo $title; ?>
        </title>
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/bootstrap-rtl.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/crm.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/gh-stylesheet.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/stylesheet.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/font-awesome.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/bootstrap-select.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/select2.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/xgrid.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/fileinput.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/bootstrap-datepicker.min.css" />
        <link rel="stylesheet" href="<?php echo asset_url(); ?>css/persian-datepicker.css" />
        <script src=" <?php echo asset_url() . 'js/jquery-1.11.1.min.js' ?>" ></script>
        <script src=" <?php echo asset_url() . 'js/bootstrap.min.js' ?>" ></script>
        <script src=" <?php echo asset_url() . 'js/grid.js' ?>" ></script>
        <script src=" <?php echo asset_url() . 'js/bootstrap-datepicker.min.js' ?>"></script>
        <script src=" <?php echo asset_url() . 'js/bootstrap-datepicker.fa.min.js' ?>"></script>
        <script src=" <?php echo asset_url() . 'js/fileinput.min.js' ?>"></script>
        <script src=" <?php echo asset_url() . 'js/select2.min.js' ?>"></script>
        <script src=" <?php echo asset_url() . 'js/jquery.PrintArea.js' ?>"></script>
        <script src=" <?php echo asset_url() . 'js/persian-date.js' ?>" ></script>
        <script src=" <?php echo asset_url() . 'js/persian-datepicker.min.js' ?>" ></script>
        <script src=" <?php echo asset_url() . 'js/javascript.js' ?>" ></script>
        <!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=fa"></script>-->
        <?php
        if (isset($has_ckeditor) && $has_ckeditor) { // if in edit_content ckedotr is activeted
            echo '<script src="' . asset_url() . 'js/ckeditor/ckeditor.js"></script>' . "\n";
        }
        ?>
    </head>
    <body>
        <div class="container-fluid">
            <!--first-header-->
            <div class="row">
                <div class="col-sm-12 gh-first-header">
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-5">
                            <div class="gh-sign-in gh-text-shadow">
                                <span data-toggle="collapse" data-target="#si">ورود به سیستم</span>
                                <form method="post" id="si" class="collapse">
                                    <ul>
                                        <li><input class="form-control" type="text" placeholder="نام کاربری"></li>
                                        <li><input class="form-control" type="password" placeholder="رمز عبور"></li>
                                        <li>
                                            <input style="width: 30%; float: left; text-align: center;" class="btn btn-success" type="button" value="ورود"> 
                                            <input style="width: 65%; float: right; text-align: center;" class="btn btn-danger" type="button" value="فراموشی رمز عبور">
                                        </li>
                                    </ul>
                                </form>
                            </div>
                            <div class="gh-sign-up gh-text-shadow">
                                <a href="#">ثبت نام</a>
                            </div>
                        </div>
                        <div class="col-sm-5"><span class="gh-quick-contact gh-text-shadow">021-44255033<img src="<?php echo asset_url(); ?>images/img/phone.png"></span></div>
                        <div class="col-sm-1"></div>
                    </div>
                </div>
            </div>
            <!--first-header-->

            <!--second-header-->
            <div class="row">
                <div class="col-sm-12 gh-second-header">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-3"><img style="margin-top: 4px; height: 82px; width: 300px;" src="<?php echo asset_url(); ?>images/img/logo.png"></div>
                    <div class="col-sm-8"></div>
                </div>
            </div>
            <!--second-header-->
            <?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$m = new menu_class();
?>
<!--menu-->
<div class="row">
    <div class="col-sm-12 gh-menu">
        <div class="row">
            <div class="col-sm-1"></div>

            <!--menu large-->
            <div class="col-sm-10 gh-menu-large hidden-sm hidden-xs">
                <ul>
                    <?php echo $m->output; ?>
                </ul>
            </div>
            <!--menu large-->

            <!--menu-small-->
<!--            <div class="col-sm-8 gh-menu-small visible-sm visible-xs">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                    منو
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <?php echo $m->output; ?>
                </ul>
            </div>-->
            <!--menu-small-->

            <div class="col-sm-1"></div>
        </div>
    </div>
</div>
<!--menu-->
</div>